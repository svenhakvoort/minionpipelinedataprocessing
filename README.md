# MinionPipelineDataprocessing

Sven Hakvoort

Data files can be found at the locations specified in the config file.
Poretools is only installed on assemblix, so the tool must be used there.

### Dependencies

Install graphmap:
```
git clone https://github.com/isovic/graphmap.git
cd graphmap
make modules
make
```


Setup centrifuge:
```
wget ftp://ftp.ccb.jhu.edu/pub/infphilo/centrifuge/downloads/centrifuge-1.0.3-beta-Linux_x86_64.zip
unzip centrifuge_zip_file -d destination_folder
cd destination_folder
wget ftp://ftp.ccb.jhu.edu/pub/infphilo/centrifuge/data/p_compressed+h+v.tar.gz
```

Setup path to the indexes
```
export CENTRIFUGE_INDEXES={path to the index}
```

Provide the path to the graphmap executable in 'config.yaml':
```
graphmap-path: "{PATH_TO_EXECUTABLE}"
```
