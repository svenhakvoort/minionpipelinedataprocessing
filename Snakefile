configfile: "config.yaml"
fast5 = config["fast5-folder"]

SAMPLE_NAMES, = glob_wildcards(fast5+"{fast}.fast5")


rule all:
    input: "centrifuge_report.png",
        "genes_report.png"

rule report:
    input: "classification.txt",
           "idxstats_report.txt"
    output:
        "centrifuge_report.png",
        "genes_report.png"
    shell:
        "Rscript reportData.R {input} {output}"

rule poretools_fastq:
    input:  fast5
    message: "Converting reads to fastq files"
    log:
        "logs/poretools/poretools_fastq.log"
    output: "reads/reads.fastq"
    shell: "poretools fastq {input} >> {output}"


rule poretools_fasta:
    input: fast5
    message: "Converting reads to fasta files"
    log:
        "logs/poretools/poretools_fasta.log"
    output: "reads/reads.fasta"
    shell: "poretools fasta {input} >> {output}"


rule map_antibiotic_resistance_genes:
    input: ab=config["ab-restistance"],
            reads="reads/reads.fasta"
    output: "mapping/mapped_reads.sam"
    log:
        "logs/mapping_graphmap/map.log"
    message: "Mapping fasta reads to {input.ab}"
    threads: 8
    shell: config["graphmap-path"]+"graphmap align -r {input.ab} -d {input.reads} -o {output}"


rule convert_sam_to_bam:
    input: "mapping/mapped_reads.sam"
    output: "mapping/mapped_reads.bam"
    message: "Converting {input} to {output}"
    log:
        "logs/mapping/convert_to_bam.log"
    threads: 8
    shell: "samtools view -Sb {input} -o {output}"


rule sort_bam:
    input: "mapping/mapped_reads.bam"
    output: "mapping/mapped_reads_sorted.bam"
    message: "Sorting {input} to {output}"
    log:
        "logs/mapping/sort_bam.log"
    threads: 8
    shell: "samtools sort {input} > {output}"


rule index_bam:
    input: "mapping/mapped_reads_sorted.bam"
    output: "mapping/mapped_reads_sorted.bam.bai"
    log:
        "logs/mapping/index_bam.log"
    message: "Index {input}, results in {output}"
    threads: 8
    shell: "samtools index {input}"


rule idxstats:
    input: "mapping/mapped_reads_sorted.bam",
            "mapping/mapped_reads_sorted.bam.bai"
    output: "idxstats_report.txt"
    log:
        "logs/mapping/idx_stats.log"
    message: "Generating results information with idxstats of {input}"
    shell: "samtools idxstats {input} > {output}"


rule centrifuge:
    input: "reads/reads.fastq"
    output: "classification.txt"
    log:
        "logs/centrifuge/centrifuge.log"
    message: "Generating results with centrifuge"
    shell: config["centrifuge-path"]+"centrifuge -q -p 8 -x p_compressed+h+v -S {output} {input}"

rule clean:
    shell:
        'rm -rf *.txt | rm -rf reads/ | rm -rf reads/ | rm -rf mapping/ | rm -rf *.tsv'