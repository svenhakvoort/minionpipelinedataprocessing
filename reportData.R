centr_dat <- read.table(argv[1], sep="\t", header=T)

map_dat <- read.table(argv[2])

png(filename=argv[3])
c_total <- sum(centr_dat$numReads)
c_found <- sum(centr_dat$numReads > 0)/c_total*100

pie(c(c_found, 100-c_found), labels=c("found", "not found"), main="Percentage of organisms found in the sample")
dev.off()

png(filename=argv[4])
total <- sum(map_dat$V3)
found <- sum(map_dat$V3 > 0)/total*100
pie(c(found, 100-found), labels=c("found", "not found"), main="Percentage of the genes which were found in the sample")
dev.off()